import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Vuetify from 'vuetify'
import VModal from 'vue-js-modal'

import {routes} from './routes'
import 'vuetify/dist/vuetify.min.css'
 
Vue.use(VModal)
Vue.use(Vuetify);
Vue.use(VueRouter);
const router = new VueRouter({routes})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
