import Home from './components/Home'
import About from './components/About'
import Courses from './components/Courses'
import Course from './components/Course'
import Contact from './components/Contact'
import SignIn from './components/SignIn'
import Dashboard from './components/backoffice/Dashboard'
import Docentes from './components/backoffice/Docentes'
import Especialidades from './components/backoffice/Especialidades'
import Cursos from './components/backoffice/Cursos'
import Alumnos from './components/backoffice/Alumnos'

export const routes = [
  {
    path:'/',
    component:Home
  },
  {
    path:'/about',
    component:About
  },
  {
    path:'/courses',
    component:Courses
  },
  {
    path:'/courses/:id',
    component: Course,
    props:true
  },
  {
    path:'/contact',
    component:Contact
  },
  {
    path:'/signin',
    component:SignIn
  },
  {
    path:'/dashboard',
    component:Dashboard,
    children:[
      {
        path:'docentes',
        component:Docentes
      },
      {
        path:'especialidades',
        component:Especialidades
      },
      {
        path:'cursos',
        component:Cursos
      },
      {
        path:'alumnos',
        component:Alumnos
      }
    ]
  }
]